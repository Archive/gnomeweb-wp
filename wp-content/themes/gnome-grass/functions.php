<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */

add_editor_style("editor_style.css");

add_theme_support('menus');

add_theme_support( 'post-thumbnails');

set_post_thumbnail_size(940, 320); // banners for home page

add_image_size( 'icon-big', 256, 256, true);
add_image_size( 'icon-small', 96, 96, true);

add_image_size( 'image-crafted-content', 420, 263, true);
add_image_size( 'thumbnail-big', 210, 210, false);
add_image_size( 'thumbnail-small', 120, 80, false);


// Custom Posts

add_action( 'init', 'create_post_type' );
function create_post_type() {
  register_post_type( 'banner',
    array(
      'labels' => array(
        'name' => 'Banners',
        'singular_name' => 'Banner',
        'add_new' => 'Add New',
        'add_new_item' => 'Add New Banner',
        'edit' => 'Edit',
        'edit_item' => 'Edit',
        'new_item' => 'New Banner',
        'view' => 'View',
        'view_item' => 'View Banner',
        'search_items' => 'Search Banners',
        'not_found' => 'No banners found',
        'not_found_in_trash' => 'No banners found in Trash',
        'parent' => 'Parent Banner',
      ),
      'public' => true,
      'exclude_from_search' => false,
      'supports' => array(
        'title', 'thumbnail', 'excerpt', 'revisions', 'author', 'custom-fields'
      )
    )
  );
  
  register_post_type( 'projects',
    array(
      'labels' => array(
        'name' => 'Projects',
        'singular_name' => 'Project',
        'add_new' => 'Add New',
        'add_new_item' => 'Add New Project',
        'edit' => 'Edit',
        'edit_item' => 'Edit',
        'new_item' => 'New Project',
        'view' => 'View',
        'view_item' => 'View Project',
        'search_items' => 'Search Projects',
        'not_found' => 'No projects found',
        'not_found_in_trash' => 'No projects found in Trash',
        'parent' => 'Parent Project',
      ),
      'public' => true,
      'exclude_from_search' => false,
      'supports' => array(
        'title', 'editor', 'thumbnail', 'excerpt', 'revisions', 'author', 'custom-fields'
      )
    )
  );
}

/*
 * Identify Ajax Language Selector
 */
if (array_key_exists('select-language', $_GET)) {
    if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest') {
        require_once('language-selector.php');
        die;
    }
}


// Beautify [galery] shortcode

add_filter('gallery_style', create_function('$a', 'return preg_replace("%
<style type=\'text/css\'>(.*?)</style>

%s", "", $a);'));

?>
